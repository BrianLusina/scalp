val baseName = "scalp"
val scalaVer = "2.13.6"
val projectVersion = "0.1"

name := baseName
version := projectVersion
scalaVersion := scalaVer

// design project
lazy val design = (project in file("design"))
  .settings(
    name := s"$baseName-design",
    version := projectVersion,
    scalaVersion := scalaVer,
    libraryDependencies ++= Seq(
      ScalaParserCombinators.core,
      ScalaTest.core,
      ScalaMock.core
    )
  )

lazy val algorithms = (project in file("algorithms"))
  .settings(
    name := s"$baseName-algorithms",
    version := projectVersion,
    scalaVersion := scalaVer,
    libraryDependencies ++= Seq(
      ScalaParserCombinators.core,
      ScalaTest.core,
      ScalaMock.core
    )
  )

lazy val datastructures = (project in file("datastructures"))
  .settings(
    name := s"$baseName-datastructures",
    version := projectVersion,
    scalaVersion := scalaVer,
    libraryDependencies ++= Seq(
      ScalaParserCombinators.core,
      ScalaTest.core,
      ScalaMock.core
    )
  )

lazy val puzzles = (project in file("puzzles"))
  .settings(
    name := s"$baseName-puzzles",
    version := projectVersion,
    scalaVersion := scalaVer,
    libraryDependencies ++= Seq(
      ScalaParserCombinators.core,
      ScalaTest.core,
      ScalaMock.core
    )
  )

lazy val math = (project in file("math"))
  .settings(
    name := s"$baseName-math",
    version := projectVersion,
    scalaVersion := scalaVer,
    libraryDependencies ++= Seq(
      ScalaParserCombinators.core,
      ScalaTest.core,
      ScalaMock.core
    )
  )

lazy val dynamicprogramming = (project in file("dynamicprogramming"))
  .settings(
    name := s"$baseName-dynamic",
    version := projectVersion,
    scalaVersion := scalaVer,
    libraryDependencies ++= Seq(
      ScalaParserCombinators.core,
      ScalaTest.core,
      ScalaMock.core
    )
  )
