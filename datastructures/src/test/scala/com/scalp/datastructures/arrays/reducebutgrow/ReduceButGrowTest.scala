package com.scalp.datastructures.arrays.reducebutgrow

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import scala.util.Random

class ReduceButGrowTest extends AnyFlatSpec with Matchers {
  val fixed = List[(List[Long], Long)](
    (List(1, 2, 3), 6),
    (List(4, 1, 1, 1, 4), 16),
    (List(2, 2, 2, 2, 2, 2), 64)
  )

  val randoms = (0 to 100).toList.map {
    _ =>
      val input =
        Random.shuffle(
          List
            .range(1, 2 + Random.nextInt(10))
            .map(_ => Random.nextInt(20).toLong)
        )
      (input, input.product)
  }

  (fixed ::: randoms)
    .distinct
    .foreach {
      case (input, expected) =>
        s"grow($input)" should s"return $expected" in {
          ReduceButGrow.grow(input) shouldBe expected
        }
    }
}
