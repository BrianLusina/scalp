import sbt._

object ScalaTest {
  val version = "3.1.0"
  val core = "org.scalatest" %% "scalatest" % version
}

object ScalaParserCombinators {
  val version = "1.1.2"
  val core = "org.scala-lang.modules" %% "scala-parser-combinators" % version % Test
}

object ScalaMock {
  val version = "5.1.0"
  val core = "org.scalamock" %% "scalamock" % version % Test
}
